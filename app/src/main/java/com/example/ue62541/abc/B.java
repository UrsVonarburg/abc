package com.example.ue62541.abc;
// Anonyme Klasse für Interface View.OnClicklistener()
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class B extends AppCompatActivity {
    int[] buttonListe =  {R.id.buttonBA, R.id.buttonBC};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);

        View.OnClickListener listener = new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                Button inputButton = (Button) v;
                String buttonText = inputButton.getText().toString();

                if ("A".equalsIgnoreCase(buttonText)) {
                    Toast.makeText(B.this, "A", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(B.this, A.class);
                    startActivity(intent);
                } else if ("C".equalsIgnoreCase(buttonText)) {
                    Toast.makeText(B.this, "C", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(B.this, C.class);
                    startActivity(intent);
                }
            }
        };
        for (int viewId : buttonListe){
            Button clickedButton = (Button) findViewById(viewId);
            clickedButton.setOnClickListener(listener);
        }
    }
}
