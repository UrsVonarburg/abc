package com.example.ue62541.abc;
//Aufruf einer Methode direkt über XML verlinkt
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class C extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c);
    }

    public void startAnActivity(View v) {
        Button inputButton = (Button) v;
        String buttonText = inputButton.getText().toString();

        if ("A".equalsIgnoreCase(buttonText)) {
            Toast.makeText(C.this, "A", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, A.class);
            startActivity(intent);

        } else if ("B".equalsIgnoreCase(buttonText)) {
            Toast.makeText(C.this, "B", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, B.class);
            startActivity(intent);
        }
    }
}
