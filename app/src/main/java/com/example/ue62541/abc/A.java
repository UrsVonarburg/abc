package com.example.ue62541.abc;

//Activity implementiert direkt interface!

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class A extends AppCompatActivity implements View.OnClickListener {
    int[] buttonListe =  {R.id.buttonAB, R.id.buttonAC};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a);

        for (int viewId : buttonListe){
            Button clickedButton = (Button) findViewById(viewId);
            clickedButton.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        Button inputButton = (Button) v;
        String buttonText = inputButton.getText().toString();

        if ("B".equalsIgnoreCase(buttonText)) {
            Toast.makeText(A.this, "B", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, B.class);
            startActivity(intent);
        } else if ("C".equalsIgnoreCase(buttonText)) {
            Toast.makeText(A.this, "C", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, C.class);
            startActivity(intent);
        }
    }
}
